class MovieRepository < Repository
  attr_reader :file_path

  def initialize(repo, file_path = 'Data/movie.json')
    super()
    @repo = repo
    @file_path = file_path
    @storage = Storage.new(@file_path)
  end

  def read_movies
    movies = @storage.json_read
    movies.map { |obj|
      @repo.movies << Movie.new(obj['title'], obj['duration'], @repo.get_genre(obj['genre_name']), obj['rating'])
    }
  end

  def add(*instances)
    super(@repo.movies, *instances)
  end

  # @return [array]
  def add_hash_to_array
    a_movies = []
    @repo.movies.map do |movie|
      a_movies << movie.get_hash
    end

    a_movies
  end

  # @param [str] file_path
  def to_json(file_path)
    File.open(file_path, 'w') { |file| file.write(JSON.dump(add_hash_to_array)) }
  end

end
