# frozen_string_literal: true

# user repository
class UserRepository < Repository
  attr_reader :file_path

  def initialize(repo, file_path = 'Data/user.json')
    super()
    @repo = repo
    @file_path = file_path
    @storage = Storage.new(@file_path)
  end

  def read_users
    users = @storage.json_read
    users.map { |obj| @repo.users << User.new(obj['name'], obj['number']) }
  end

  # @param [str] file_path
  def to_json(file_path)
    File.open(file_path, 'w') { |file| file.write(JSON.dump(add_hash_to_array)) }
  end

  # @return [array]
  def add_hash_to_array
    a_users = []
    @repo.users.map do |user|
      a_users << user.get_hash
    end

    a_users
  end


end
