# frozen_string_literal: true

# data getting tickets
class TicketRepository < Repository
  attr_reader :file_path

  require 'json'
  require 'date'

  def initialize(repo, file_path = 'Data/ticket.json')
    super()
    @repo = repo
    @file_path = file_path
    @storage = Storage.new(@file_path)
  end

  def read_tickets
    tickets = @storage.json_read
    tickets.map do |obj|
      @repo.tickets << Ticket.new(
        @repo.get_movie(obj['movie_title']),
        @repo.get_user(obj['user_name']),
        obj['price'],
        obj['dateTime'].nil? ? nil : DateTime.strptime(obj['dateTime'], '%Y-%m-%d %H:%M:%S')
      )
    end
  end

  def get_sum(month, year)
    sum = 0
    @repo.tickets.map do |ticket|
      next if ticket.date.nil?

      sum += ticket.price if ticket.date.year == year && ticket.date.month == month
    end
    sum
  end

  def add(*instances)
    super(@repo.tickets, *instances)
  end

  # @return [array]
  def add_hash_to_array
    a_tickets = []
    @repo.tickets.map do |ticket|
      a_tickets << ticket.get_hash
    end

    a_tickets
  end
end
