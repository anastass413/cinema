class GenreRepository
  attr_reader :file_path

  def initialize(repo, file_path = 'Data/genre.json')
    super()
    @repo = repo
    @file_path = file_path
    @storage = Storage.new(@file_path)
  end

  def read_genres
    genres = @storage.json_read
    genres.map { |obj| @repo.genres << Genre.new(obj['name'], obj['description']) }
  end

  # @return [array]
  def add_hash_to_array
    a_genres = []
    @repo.genres.map do |genre|
      a_genres << genre.get_hash
    end

    a_genres
  end

  # @param [str] file_path
  def to_json(file_path)
    File.open(file_path, 'w') { |file| file.write(JSON.dump(add_hash_to_array)) }
  end

end