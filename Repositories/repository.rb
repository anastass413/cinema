# frozen_string_literal: true

# crud operations
class Repository
  require './Models/user'
  require './Models/ticket'
  require './Models/movie'
  require './Models/genre'
  require './storage'

  attr_reader :genres, :movies, :tickets, :users

  def initialize
    @genres = []
    @users = []
    @tickets = []
    @movies = []
  end

  def get_user(user_name)
    @users.map do |user|
      return user if user.name == user_name
    end
    raise 'User not found'
  end

  def get_movie(movie_title)
    @movies.map do |movie|
      return movie if movie.title == movie_title
    end
    raise 'Movie not found'
  end

  def get_genre(genre_name)
    return nil if genre_name.nil?

    @genres.map do |genre|
      return genre if genre.name == genre_name
    end
    raise 'Genre not found'
  end

  def add_genre(*instances)
    name_list = []
    @genres&.map { |obj| name_list << obj.name }

    instances.map do |instance|
      if name_list.include?(instance.name)
        raise "#{instance} already exists"
      else
        @genres << instance
      end
    end
  end

  def add(storage, *instances)
    instances.map { |instance| storage << instance }
  end

  def crud_delete(array, *instances)
    instances.map! { |instance| array.delete_at(array.index(instance)) }
  end

  def crud_read(array, *instances)
    instances.map do |instance|
      array[array.index(instance)].inspect
    end
  rescue TypeError
    "#{array}\n storage does not contain such element"
  end


end
