# frozen_string_literal: true

# movies validation
class Movie

  attr_reader :title, :genre, :duration, :rating

  def initialize(title, duration, genre = nil, rating = nil)
    @title = title_validation(title)
    @duration = duration_validation(duration)
    @genre = genre_validation(genre)
    @rating = rating_validation(rating)
  end

  # @return [Hash]
  def get_hash
    h_movies = {}
    h_movies['title'] = @title
    h_movies['duration'] = @duration
    h_movies['genre_name'] = @genre.nil? ? nil : @genre.name
    h_movies['rating'] = @rating

    h_movies
  end

  def to_json(*_args)
    {
      "title": @title,
      "duration": @duration,
      "genre_name": @genre.name,
      "rating": @rating
    }.to_json
  end

  def title_validation(title)
    raise 'You should enter non-empty string' unless title.is_a? String

    title
  end

  def duration_validation(duration)
    raise 'You should enter int number' unless duration.is_a? Integer

    duration
  end

  def genre_validation(genre)
    raise 'You should enter Genre instance' unless ((genre.is_a? Genre) || genre.nil?)

    genre
  end

  def rating_validation(rating)
    raise 'You should enter float number' unless (rating.is_a? Float) || rating.nil?

    raise 'Number must be in [0.0-10.0] range' if !rating.nil? && !rating.between?(0.0, 10.0)

    rating
  end

end
