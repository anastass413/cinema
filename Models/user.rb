# frozen_string_literal: true

# users validation
class User
  @@regexp_phone = /^(0\S{2}) (\S{3}) (\S{2}) (\S{2})/


  attr_reader :name, :number

  def initialize(name, number = nil)
    @name = name_validation(name)
    @number = number_validation(number)
  end

  # @return [Hash]
  def get_hash
    h_users = {}
    h_users['name'] = @name
    h_users['number'] = @number

    h_users
  end

  def to_json(*_args)
    {
      "name": @name,
      "number": @number
    }.to_json
  end


  def name_validation(name)
    if !(name.is_a? String) || name == ''
      raise 'Name should be a string value'
    else
      @name = name
    end
  end

  def number_validation(number)
    if @@regexp_phone.match?(number) && !number.nil?
      raise 'Please enter a string using following format:\n(0xx) xxx xx xx'
    end

    @number = number

  end

end
