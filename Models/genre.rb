# frozen_string_literal: true

# genres validation
class Genre

  attr_reader :name

  def initialize(name, description = nil)
    @name = name_validation(name)
    @description = description_validation(description)
  end

  def to_json(*_args)
    {
      "name": @name,
      "description": @description
    }.to_json
  end

  # @return [Hash]
  def get_hash
    h_genres = {}
    h_genres['name'] = @name
    h_genres['description'] = @description

    h_genres
  end

  def name_validation(name)
    raise 'You should enter non-empty value' unless name != ''
    raise 'You should enter String value' unless name.is_a? String

    name
  end

  def description_validation(description)
    raise 'You should enter String value' unless (description.is_a? String) || description.nil?

    description
  end
end
