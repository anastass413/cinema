# frozen_string_literal: true

# tickets validation
class Ticket

  attr_reader :movie, :user, :date, :price

  def initialize(movie, user, price, date = nil)
    @movie = movie_validation(movie)
    @user = user_validation(user)
    @price = price_validation(price)
    @date = date_validation(date)
  end

  def to_json(*_args)
    {
      "movie_title": @movie.title,
      "user_name": @user.name,
      "price": @price,
      "dateTime": @date
  }.to_json
  end

  # @return [Hash]
  def get_hash
    h_tickets = {}
    h_tickets['movie_title'] = @movie.title
    h_tickets['user_name'] = @user.name
    h_tickets['price'] = @price
    h_tickets['dateTime'] = @date.nil? ? nil : @date.strftime('%Y-%m-%d %H:%M:%S')

    h_tickets
  end

  def movie_validation(movie)
    raise 'You should enter a Movie class instance' unless movie.is_a? Movie

    movie
  end

  def user_validation(user)
    raise 'You should enter a User class instance' unless user.is_a? User

    user
  end

  def date_validation(date = DateTime.today)
    raise 'You should enter a Date class instance' unless (date.is_a? DateTime) || date.nil?

    date
  end

  def price_validation(price)
    raise 'You should enter a Float num' unless price.is_a? Float

    price
  end
end
