# frozen_string_literal: true

# data adding
class Storage
  require './Models/user'
  require './Models/ticket'
  require './Models/movie'
  require './Models/genre'
  require './storage'
  require 'json'

  attr_reader :file_path, :parsed

  def initialize(file_path)
    @file_path = file_path
  end

  def json_read
    text = File.read(@file_path)
    @parsed = { '' => '' } if text.length.zero?
    JSON.parse(text) # {"name"=>"User1", "number"=>"(099) 374 99 32"}
  end

  def json_write
    File.write(@file_path)
  end

end
