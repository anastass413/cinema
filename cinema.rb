# frozen_string_literal: true

# main file
module Cinema
  require './Repositories/repository'
  require './Repositories/user_repository'
  require './Repositories/ticket_repository'
  require './Repositories/movie_repository'
  require './Repositories/genre_repository'
  require_relative 'Models/user'
  require_relative 'Models/ticket'
  require_relative 'Models/movie'
  require_relative 'Models/genre'

  repo = Repository.new

  test_genre_repository = GenreRepository.new(repo)
  test_genre_repository.read_genres

  test_user_repository = UserRepository.new(repo)
  test_user_repository.read_users

  test_movie_repository = MovieRepository.new(repo)
  test_movie_repository.read_movies

  test_ticket_repository = TicketRepository.new(repo)
  test_ticket_repository.read_tickets

  puts test_ticket_repository.get_sum(2021)


end
